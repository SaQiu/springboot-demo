package com.example.demo.redis;

import org.junit.Test;

import com.alibaba.fastjson.JSONObject;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

public class RedisTest {
	
	@Test
	public void getJedis() {
		// 1、 new Jedis 对象即可
		Jedis jedis = new Jedis("192.168.0.101",6379);
		// jedis 所有的命令就是我们之前学习的所有指令！所以之前的指令学习很重要！
		System.out.println(jedis.ping());
	}
	
	@Test
	public void getJedisTx() {
		Jedis jedis = new Jedis("192.168.0.101", 6379);
		jedis.flushDB();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("hello","world");
		jsonObject.put("name","kuangshen");
		// 开启事务
		Transaction multi = jedis.multi();
		String result = jsonObject.toJSONString();
//		bilibili：狂神说Java
//		SpringBoot整合
//		SpringBoot 操作数据：spring-data jpa jdbc mongodb redis！
//		SpringData 也是和 SpringBoot 齐名的项目！
//		说明： 在 SpringBoot2.x 之后，原来使用的jedis 被替换为了 lettuce?
//		jedis : 采用的直连，多个线程操作的话，是不安全的，如果想要避免不安全的，使用 jedis pool 连接
//		池！ 更像 BIO 模式
//		lettuce : 采用netty，实例可以再多个线程中进行共享，不存在线程不安全的情况！可以减少线程数据
//		了，更像 NIO 模式
//		源码分析：
		// jedis.watch(result)
		try {
		multi.set("user1",result);
		multi.set("user2",result);
		int i = 1/0 ; // 代码抛出异常事务，执行失败！
		multi.exec(); // 执行事务！
		} catch (Exception e) {
		multi.discard(); // 放弃事务
		e.printStackTrace();
		} finally {
		System.out.println(jedis.get("user1"));
		System.out.println(jedis.get("user2"));
		jedis.close(); // 关闭连接
		}
	}

}
