package com.example.demo.minio;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.RemoveObjectArgs;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.RegionConflictException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

@Configuration
@EnableConfigurationProperties({MinioProperties.class})
public class MinioTemplate  {
	
	@Autowired
	private MinioProperties minioProperties;

    private MinioClient minioClient ;
    
    @Bean
    public MinioClient  minioClient() {
    	return  minioClient =
    		    MinioClient.builder()
    		        .endpoint(minioProperties.getUrl())
    		        .credentials(minioProperties.getAccessKey(), minioProperties.getSecretKey())
    		        .build();
    	 
    	 

    }

    /**
     * 判断 bucket是否存在
     * @param bucketName
     * @return
     */
    public boolean bucketExists(String bucketName) throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, IOException{
    	boolean found = 
    	    	  minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
    	    	if (found) {
    	    	  System.out.println("my-bucketname exists");
    	    	} else {
    	    	  System.out.println("my-bucketname does not exist");
    	    	}
    			return found;
    }

    /**
     * 创建 bucket
     * @throws RegionConflictException 
     */
    public void makeBucket(String bucketName) throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, IOException, RegionConflictException{
    	// Create bucket with default region.
    	minioClient.makeBucket(
    	    MakeBucketArgs.builder()
    	        .bucket(bucketName)
    	        .build());

    }

    /**
     * 文件上传
     */
    public void putObject(String bucketName, String objectName, InputStream stream) throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, IOException{
    	minioClient.putObject(
    		    PutObjectArgs.builder().bucket(bucketName).object(objectName).stream(
    		    		stream, -1, 10485760)
    		        .contentType("video/mp4")
    		        .build());
    }
    /**
     * 删除文件
     */
    public void removeObject(String bucketName, String objectName) throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, IOException{
    	// Remove object.
    	minioClient.removeObject(
    	    RemoveObjectArgs.builder().bucket(bucketName).object(objectName).build());
    }
    
    /**
     * 获取文件路径
     * @param bucketName
     * @param objectName
     * @return
     */
    public String  getObjectUrl(String bucketName, String objectName ) throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, IOException {
    	String url = minioClient.getObjectUrl(bucketName, objectName);
    	return url;

    }

	
}
