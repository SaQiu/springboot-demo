package com.example.demo.minio;

import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/test")
public class MinioController {
	
	@Autowired
	MinioTemplate minioTemplate;
	
	@ResponseBody
	@RequestMapping("/upload")
	public String upload(@RequestParam("file") MultipartFile data,String bucketName,String path){
		String fileName = data.getOriginalFilename();
		System.out.println(fileName);
		InputStream inputStram;
		try {
			inputStram = data.getInputStream();
			minioTemplate.putObject(bucketName,fileName,inputStram);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	}
}
