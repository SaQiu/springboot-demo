package com.example.demo.minio;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@ConfigurationProperties(prefix = "minio")
@Configuration
public class MinioProperties {
   /**
    * minio 服务地址 http://ip:port
    */
   private String url;
   /**
    * 用户名
    */
   private String accessKey;
   /**
    * 密码
    */
   private String secretKey;
    /**
    * 桶名称
    */
   private String bucketName;
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getAccessKey() {
		return accessKey;
	}
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
	public String getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
	public String getBucketName() {
		return bucketName;
	}
	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}
   
}

