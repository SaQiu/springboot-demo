package com.example.demo.ESTest;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ItemServiceImpl implements ItemService {
	
    @Autowired
    ItemRepository itemRepository;

    @Override
    public Iterable<Item> findAll() {

        return itemRepository.findAll();
    }


}




