package com.example.demo.controller;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import javax.imageio.ImageIO;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
/**
 * 画制定logo和制定描述的二维码
 * 二维码 200*200
 * 背景 400*600
 * @author 
 */
public class qrCode {
    private static final int QRCOLOR = 000000;// 0x830B0D; // 深红色
    private static final int BGWHITE = 0xFFFFFF; // 背景颜色

    private static final int WIDTH = 250; // 二维码宽
    private static final int HEIGHT = 250; // 二维码高

    // 用于设置QR二维码参数
    private static Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>() {
        private static final long serialVersionUID = 1L;
        {
            put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);// 设置QR二维码的纠错级别（H为最高级别）具体级别信息
            put(EncodeHintType.CHARACTER_SET, "utf-8");// 设置编码方式
            put(EncodeHintType.MARGIN, 0);//白边？？
        }
    };

    public static void main(String[] args)  {
        File logoFile = new File("F:/ga.png");//logo图片
        File bgImgFile=new File("F:/bg.png");//背景图片
        String [] strPath=new String[] {"小店分局平阳路派出所"};
        String [] codes=new String[] {"140105140000"};
        //String [] strPath=new String[] {"阳曲县局侯村派出所","阳曲县局北小店派出所","阳曲县局高村派出所","阳曲县局凌井店派出所","阳曲县局黄寨派出所","阳曲县局大盂派出所","阳曲县局泥屯派出所","阳曲县局东黄水派出所","阳曲县局西凌井派出所","阳曲县局杨兴派出所","迎泽分局老军营派出所","迎泽分局桥东派出所","迎泽分局文庙派出所","迎泽分局庙前派出所","迎泽分局郝庄派出所","迎泽分局长风东派出所","迎泽分局迎泽派出所","迎泽分局柳巷派出所","西山分局虎窝派出所","西山分局虎峪派出所","西山分局北寒派出所","西山分局金牛派出所","西山分局雁门派出所","西山分局西塔派出所","西山分局屯川派出所","西山分局滨西派出所","西山分局九院派出所","西山分局滨东派出所","综改分局教育园区派出所","综改分局中环派出所","综改分局唐明派出所","综改分局富士康园区派出所","综改分局第二分局公交派出所","综改分局第二分局长途客运派出所","综改分局工业园派出所","综改分局五龙湾派出所","直属分局财苑派出所","直属分局新北派出所","直属分局向阳派出所","直属分局新西派出所","直属分局星光派出所","直属分局长安派出所","直属分局中医苑派出所","直属分局胜利派出所","直属分局党苑派出所","直属分局梅山派出所","直属分局劲松派出所","直属分局学苑派出所","直属分局科苑派出所","直属分局北苑派出所","直属分局师苑派出所","直属分局医科苑派出所","直属分局源化派出所","直属分局银燕派出所","直属分局汾西派出所","直属分局新兰派出所","直属分局工苑派出所","清徐县公安局西谷派出所","清徐县公安局马峪派出所","清徐县公安局清源派出所","清徐县公安局徐沟派出所","清徐县公安局孟封派出所","清徐县公安局王答派出所","清徐县公安局柳杜派出所","清徐县公安局东湖派出所","清徐县公安局东于派出所","清徐县公安局集义派出所","清徐县公安局清泉湖派出所","清徐县公安局醋都派出所","杏花岭分局鼓楼派出所","杏花岭分局龙潭派出所","杏花岭分局大东关派出所","杏花岭分局杏花岭派出所","杏花岭分局巨轮派出所","杏花岭分局小返派出所","杏花岭分局中涧河派出所","杏花岭分局三桥派出所","杏花岭分局坝陵桥派出所","杏花岭分局敦化坊派出所","杏花岭分局涧河派出所","杏花岭分局职工新村派出所","杏花岭分局杨家峪派出所","晋源分局晋祠派出所","晋源分局金胜派出所","晋源分局姚村派出所","晋源分局罗城派出所","晋源分局义井派出所","晋源分局晋源派出所","尖草坪分局柴村派出所","尖草坪分局马头水派出所","尖草坪分局迎新街派出所","尖草坪分局不锈钢工业园区派出所","尖草坪分局柏板派出所","尖草坪分局光社派出所","尖草坪分局西墕派出所","尖草坪分局古城派出所","尖草坪分局尖草坪派出所","尖草坪分局上兰派出所","尖草坪分局新城派出所","尖草坪分局南寨派出所","尖草坪分局汇丰派出所","尖草坪分局阳曲派出所","小店分局小店派出所","小店分局亲贤派出所","小店分局北格派出所","小店分局龙城派出所","小店分局刘家堡派出所","小店分局黄陵派出所","小店分局坞城派出所","小店分局平阳路派出所","小店分局王村派出所","小店分局营盘派出所","小店分局西温庄派出所","小店分局北营派出所","娄烦县局庙湾派出所","娄烦县局米峪镇派出所","娄烦县局天池店派出所","娄烦县局马家庄派出所","娄烦县局静游派出所","娄烦县局城关派出所","娄烦县局下石庄派出所","娄烦县局杜交曲派出所","娄烦县局盖家庄派出所","城北分局南区派出所","城北分局东区派出所","城北分局北区派出所","城北分局尖山派出所","城北分局峨口派出所","城北分局大关山派出所","城北分局袁家庄派出所","城北分局西区派出所","古交市局原相派出所","古交市局邢家社派出所","古交市局阁上派出所","古交市局镇城底派出所","古交市局东曲派出所","古交市局马兰派出所","古交市局梭峪派出所","古交市局常安派出所","古交市局西曲派出所","古交市局岔口派出所","古交市局屯兰派出所","古交市局河口派出所","古交市局城管派出所","古交市局嘉乐泉派出所","古交市局桃园派出所","万柏林分局和平北路派出所","万柏林分局玉河派出所","万柏林分局杜儿坪派出所","万柏林分局建矿派出所","万柏林分局兴华小区派出所","万柏林分局西铭派出所","万柏林分局王封派出所","万柏林分局长风派出所","万柏林分局和平南路派出所","万柏林分局千峰派出所","万柏林分局万柏林派出所","万柏林分局丽华派出所","万柏林分局龙泉派出所","万柏林分局化客头派出所","万柏林分局白家庄派出所","万柏林分局小井峪派出所","万柏林分局西华苑派出所","万柏林分局东社派出所","第二分局长途客运派出所","第二分局公交派出所"};
		//String [] codes = new String[] {"140122210000","140122250000",	"140122220000",	"140122230000",	"140122170000",	"140122190000",	"140122180000",	"140122200000",	"140122240000",	"140122260000",	"140106170000",	"140106120000",	"140106160000",	"140106150000",	"140106180000",	"140106190000",	"140106130000",	"140106140000",	"140193110000",	"140193100000",	"140193090000",	"140193180000",	"140193170000",	"140193160000",	"140193150000",	"140193140000",	"140193120000",	"140193130000",	"140198070000",	"140199050000",	"140198050000",	"140198060000",	"140195100000",	"140195090000",	"140197070000",	"140197060000",	"140196220000",	"140196300000",	"140196150000",	"140196290000",	"140196140000",	"140196130000",	"140196280000",	"140196120000",	"140196270000",	"140196100000",	"140196090000",	"140196200000",	"140196260000",	"140196250000",	"140196240000",	"140196230000",	"140196160000",	"140196170000",	"140196180000",	"140196190000",	"140196210000",	"140121320000",	"140121290000",	"140121240000",	"140121250000",	"140121270000",	"140121280000",	"140121300000",	"140121340000",	"140121260000",	"140121310000",	"140121330000",	"140121335000",	"140107140000",	"140107240000",	"140107200000",	"140107150000",	"140107120000",	"140107230000",	"140107220000",	"140107130000",	"140107160000",	"140107170000",	"140107180000",	"140107190000",	"140107210000",	"140110160000",	"140110140000",	"140110170000",	"140110130000",	"140110120000",	"140110150000",	"140108180000",	"140108250000",	"140108150000",	"140108260000",	"140108230000",	"140108190000",	"140108240000",	"140108160000",	"140108130000",	"140108200000",	"140108170000",	"140108140000",	"140108120000",	"140108220000",	"140105200000",	"140105150000",	"140105220000",	"140105170000",	"140105230000",	"140105190000",	"140105160000",	"140105140000",	"140105130000",	"140105120000",	"140105210000",	"140105180000",	"140123270000",	"140123260000",	"140123230000",	"140123220000",	"140123210000",	"140123200000",	"140123280000",	"140123240000",	"140123250000",	"140194120000",	"140194090000",	"140194110000",	"140194130000",	"140194140000",	"140194150000",	"140194160000",	"140194100000",	"140181340000",	"140181290000",	"140181330000",	"140181270000",	"140181210000",	"140181250000",	"140181300000",	"140181350000",	"140181230000",	"140181320000",	"140181260000",	"140181280000",	"140181240000",	"140181310000",	"140181220000",	"140109170000",	"140109180000",	"140109270000",	"140109200000",	"140109150000",	"140109210000",	"140109290000",	"140109120000",	"140109130000",	"140109160000",	"140109190000",	"140109230000",	"140109250000",	"140109260000",	"140109280000",	"140109140000",	"140109240000",	"140109220000","140195090000","140195100000"};
		for(int i=0;i<strPath.length;i++) {
//			String url = "http://rk.gaj.taiyuan.gov.cn/rkapp/login?pcs="+codes[i];
			String url = "http://rk.gaj.taiyuan.gov.cn/wxAuth/login?pcs="+codes[i];
			File QrCodeFile = new File("F:/"+strPath[i]+".png");//生成图片位置
			drawLogoQRCode(logoFile, QrCodeFile,bgImgFile, url, strPath[i]);
		}
        
		
    }

    // 生成带logo的二维码图片
    public static void drawLogoQRCode(File logoFile, File codeFile, File bgImgFile,String qrUrl, String note) {
        try {
            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
            // 参数顺序分别为：编码内容，编码类型，生成图片宽度，生成图片高度，设置参数
            BitMatrix bm = multiFormatWriter.encode(qrUrl, BarcodeFormat.QR_CODE, WIDTH, HEIGHT, hints);
            BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);

            // 开始利用二维码数据创建Bitmap图片，分别设为黑（0xFFFFFFFF）白（0xFF000000）两色
            for (int x = 0; x < WIDTH; x++) {
                for (int y = 0; y < HEIGHT; y++) {
                    image.setRGB(x, y, bm.get(x, y) ? QRCOLOR : BGWHITE);
                }
            }
            /**
             * 增加logo
             */
            int width = image.getWidth();
            int height = image.getHeight();
            if (Objects.nonNull(logoFile) && logoFile.exists()) {
                // 构建绘图对象
                Graphics2D logoPlus = image.createGraphics();
                // 读取Logo图片
                BufferedImage logo = ImageIO.read(logoFile);
                // 开始绘制logo图片
                logoPlus.drawImage(logo, width * 2 / 5, height * 2 / 5, width * 2 / 10, height * 2 / 10, null);
                logoPlus.dispose();
                logo.flush();
            }

            /**
             * 增加背景图片   
             */
            BufferedImage backgroundImage = ImageIO.read(bgImgFile);
            int bgWidth=backgroundImage.getWidth();
            //int bgHeight=backgroundImage.getHeight();
            int qrWidth=image.getWidth();
            //距离背景图片x边的距离，居中显示
            int disx=(bgWidth-qrWidth)/2;
            //距离y边距离
            int disy=393;
            //字体间距
            double rate=1.2;
            Graphics2D rng=backgroundImage.createGraphics();
            rng.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP));
            rng.drawImage(image,disx,disy,WIDTH,HEIGHT,null);

            /**
             * 增加文字描述
             */
            Color textColor=Color.white;
            rng.setColor(textColor);
            rng.drawImage(backgroundImage,0,0,null);
            //设置字体
            rng.setFont(new Font("微软雅黑",Font.BOLD,20));
            int strWidth=rng.getFontMetrics().stringWidth(note);
            //文字居中显示
            int disx1=(int)(bgWidth-strWidth*rate)/2;
            //设置间距
            MyDrawString(note, disx1, 674, rate, rng);
//            rng.drawString(note,disx1,624);
            rng.dispose();
            image=backgroundImage;
            image.flush();
            ImageIO.write(image, "png", codeFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /*调整字体间距
     * */
    public static void MyDrawString(String str,int x,int y,double rate,Graphics g){
		String tempStr=new String();
		int orgStringWight=g.getFontMetrics().stringWidth(str);
		int orgStringLength=str.length();
		int tempx=x;
		int tempy=y;
		while(str.length()>0)
		{
			tempStr=str.substring(0, 1);
			str=str.substring(1, str.length());
			g.drawString(tempStr, tempx, tempy);
			tempx=(int)(tempx+(double)orgStringWight/(double)orgStringLength*rate);
		}
    }
}