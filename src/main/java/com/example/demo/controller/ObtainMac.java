package com.example.demo.controller;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class ObtainMac {

	private static String getLocalMacAddressFromIp() {
		 String strMacAddr = null;
		 try {
		  //获得IpD地址
		  InetAddress ip = getLocalInetAddress();
		  byte[] b = NetworkInterface.getByInetAddress(ip).getHardwareAddress();
		  StringBuffer buffer = new StringBuffer();
		  for (int i = 0; i < b.length; i++) {
		  if (i != 0) {
		   buffer.append(':');
		  }
		  String str = Integer.toHexString(b[i] & 0xFF);
		  buffer.append(str.length() == 1 ? 0 + str : str);
		  }
		  strMacAddr = buffer.toString().toUpperCase();
		 } catch (Exception e) {
		 }
		 return strMacAddr;
		 }
	
	 private static InetAddress getLocalInetAddress() {
	 InetAddress ip = null;
	 try {
	  //列举
	  Enumeration<NetworkInterface> en_netInterface = NetworkInterface.getNetworkInterfaces();
	  while (en_netInterface.hasMoreElements()) {//是否还有元素
	  NetworkInterface ni = (NetworkInterface) en_netInterface.nextElement();//得到下一个元素
	  Enumeration<InetAddress> en_ip = ni.getInetAddresses();//得到一个ip地址的列举
	  while (en_ip.hasMoreElements()) {
	   ip = en_ip.nextElement();
	   if (!ip.isLoopbackAddress() && ip.getHostAddress().indexOf(":") == -1)
	   break;
	   else
	   ip = null;
	  }
	  if (ip != null) {
	   break;
	  }
	  }
	 } catch (SocketException e) {
	  e.printStackTrace();
	 }
	 return ip;
	 }


	public static void main(String[] args) {
		String localMacAddressFromIp = getLocalMacAddressFromIp();
		System.out.println(localMacAddressFromIp);
	}

}
