package com.example.demo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.cluster.metadata.AliasMetaData;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder.Field;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.SearchResultMapper;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.aggregation.impl.AggregatedPageImpl;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.ESTest.Item;
import com.example.demo.ESTest.ItemRepository;
import com.example.demo.ESTest.ItemService;

@Controller
public class TestController {
	
	@Autowired
	ItemService itemService;
	
	@Autowired
    private ElasticsearchTemplate elasticsearchTemplate;
	@Autowired
    private ItemRepository itemRepository;
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@ResponseBody
	@GetMapping("/hello")
	public Map<String ,Object> hello() {
		Map<String ,Object> map=new HashMap<String,Object>();
		map.put("name","lucy" );
		map.put("age", 23);
		return map;
		
	}
	@RequestMapping("/test")
	public String test(String message) {
		return "index";
		
	}
	//http://localhost:9090/findKeyword?keyword=item
	@ResponseBody
	@RequestMapping("findKeyword")
    public List<AliasMetaData> findKeyword(String keyword) {

        return elasticsearchTemplate.queryForAlias(keyword);
    }
	//单字符串查询
	@ResponseBody
	@RequestMapping("/singleWord")
    public List<Item>  singleTitle(String word, @PageableDefault Pageable pageable) {
        //使用queryStringQuery完成单字符串查询
        SearchQuery searchQuery = new NativeSearchQueryBuilder().withQuery(QueryBuilders.queryStringQuery(word)).withPageable(pageable).build();
        return elasticsearchTemplate.queryForList(searchQuery, Item.class);
    }
	//multi_match多个字段匹配某字符串
	@ResponseBody
	@RequestMapping("/multiMatch")
    public Object singleUserId(String title, @PageableDefault Pageable pageable) {
        SearchQuery searchQuery = new NativeSearchQueryBuilder().withQuery(QueryBuilders.multiMatchQuery(title, "title", "brand")).withPageable(pageable).build();
        return elasticsearchTemplate.queryForList(searchQuery, Item.class);
	}
	
	//分页查询
	@ResponseBody
	@RequestMapping("/searchByPage")
    public Object searchByPage(String title,int page) {
		 // 构建查询条件
        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();
        // 添加基本分词查询
        queryBuilder.withQuery(QueryBuilders.termQuery("title", title));
        // 分页：
        //int page = 0;
        int size = 2;
        withHighlight(queryBuilder);
        queryBuilder.withPageable(PageRequest.of(page, size));
        // 搜索，获取结果
        Page<Item> items = itemRepository.search(queryBuilder.build());
        return items;
		}
	
	//分页查询
		@ResponseBody
		@RequestMapping("/searchByPage2")
	    public Page<Item>  searchByPage2(String title,int page) {
			 // 构建查询条件
	        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();
	        // 添加基本分词查询
	        queryBuilder.withQuery(QueryBuilders.termQuery("title", title));
	        // 分页：
	        //int page = 0;
	        int size = 2;
	        //withHighlight(queryBuilder);
	        
	    	// 高亮设置
			List<String> highlightFields = new ArrayList<String>();
			highlightFields.add("title");
			highlightFields.add("category");
			highlightFields.add("brand");
			Field[] fields = new Field[highlightFields.size()];
			for (int x = 0; x < highlightFields.size(); x++) {
				fields[x] = new HighlightBuilder.Field(highlightFields.get(x)).preTags("<em style='color:red'>")
						.postTags("</em>");
			}
			queryBuilder.withHighlightFields(fields);
	        
	        queryBuilder.withPageable(PageRequest.of(page, size));
	        
	        //自定义查询结果封装  	
	        AggregatedPage<Item> resultPage = elasticsearchTemplate.queryForPage(queryBuilder.build(), Item.class,  
	        new SearchResultMapper() {

				@Override
				public <T> AggregatedPage<T> mapResults(SearchResponse searchResponse, Class<T> clazz, Pageable pageable) {
					 //先来一个容器把数据装起来...
	                List<Item> itemList = new ArrayList<>();
	                //我们所有查询的结果都放在这个searchResponse里面
	                //我们现在就是要把我们想要的内容从这个searchResponse里面获取到
	                SearchHits hits = searchResponse.getHits();
	                //如果getTotalHits是0 则表示查询不到数据
	                if (hits.getTotalHits() <= 0) {
	                    return null;
	                } else {
	                    //从里面获取到一条一条的数据了
	                    for (SearchHit hit : hits) {
	                    	Item item = new Item();
	                        //还要获取到某个字段的高亮特征 高亮的特征和当前的数据 做一个替换
	                        HighlightField companyHighlight = hit.getHighlightFields().get("title");
	                        if (companyHighlight != null) {
	                        	item.setTitle(companyHighlight.fragments()[0].toString());
	                        } else {
	                            String title = (String) hit.getSourceAsMap().get("title");
	                            item.setTitle(title);
	                        }
	                        itemList.add(item);
	                    }
	                }
	                return new AggregatedPageImpl(itemList);
				}

				@Override
				public <T> T mapSearchHit(SearchHit searchHit, Class<T> type) {
					return null;
				}  
	          
	        });  
	        return resultPage;
			}
	
	//http://localhost:9090/findAll
	@ResponseBody
	@RequestMapping("findAll")
    public Iterable<Item> findAll() {

        return itemService.findAll();
    }
	
	private void withHighlight(NativeSearchQueryBuilder searchQuery){
		Field hfield= new HighlightBuilder.Field("title")
				 .preTags("<em style='color:red'>")
				 .postTags("</em>")
				 .fragmentSize(100);
		searchQuery.withHighlightFields(hfield);
	}


}
